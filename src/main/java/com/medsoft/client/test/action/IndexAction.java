package com.medsoft.client.test.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.medsoft.base.utils.JsonUtils;
import com.medsoft.drpcp.api.DemoDrpcpService;
import com.medsoft.drpcp.api.DemoQuery;
import com.medsoft.drpcp.api.DemoReq;


/**
 * Created with IntelliJ IDEA.
 * User: zjhua
 * Date: 2015-01-02
 * Time: 23:56
 * Mail: zjhua@hundsun.com
 * Comment: 功能描述
 * Modifiy History: 修改历史
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class IndexAction {
	@Autowired(required=false)
	private DemoDrpcpService demoDrpcpService;
	
	@RequestMapping(value="/index.html",method=RequestMethod.GET)
	public @ResponseBody String index(Model model,HttpServletRequest request){
		DemoQuery req = new DemoQuery();
		DemoReq param = new DemoReq();
		req.setDrpcpVersion("v1.1");
		param.setId(8888);
		param.setName("zhangsan");
		req.setParam(param);
		//model.addAttribute("drpcpResp",JsonUtils.toJson(demoDrpcpService.queryDrpcpService(req)));
		return JsonUtils.toJson(demoDrpcpService.queryDrpcpService(req));
	}
	
	@RequestMapping(value="/index_v1.html",method=RequestMethod.GET)
	public @ResponseBody String indexV1(Model model,HttpServletRequest request){
		DemoQuery req = new DemoQuery();
		DemoReq param = new DemoReq();
		req.setDrpcpVersion("v1.1");
		param.setId(8888);
		param.setName("zhangsan");
		req.setParam(param);
		//model.addAttribute("drpcpResp",JsonUtils.toJson(demoDrpcpService.queryDrpcpService(req)));
		return JsonUtils.toJson(demoDrpcpService.queryDrpcpService(req));
	}
	
	@RequestMapping(value="/index_v2.html",method=RequestMethod.GET)
	public @ResponseBody String indexV2(Model model,HttpServletRequest request){
		DemoQuery req = new DemoQuery();
		DemoReq param = new DemoReq();
		req.setDrpcpVersion("v1.2");
		param.setId(9999);
		param.setName("wangsi");
		req.setParam(param);
		//model.addAttribute("drpcpResp",JsonUtils.toJson(demoDrpcpService.queryDrpcpService(req)));
		return JsonUtils.toJson(demoDrpcpService.queryDrpcpService(req));
	}
}

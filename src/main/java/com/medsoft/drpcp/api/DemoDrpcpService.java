package com.medsoft.drpcp.api;

import com.medsoft.drpcp.annotation.Service;
import com.medsoft.drpcp.annotation.ServiceModule;

@ServiceModule
public interface DemoDrpcpService {
	@Service(desc = "Drpcp修改", serviceId = "99000001")
	public DemoResp opDrpcpService(DemoReq req);
	@Service(desc = "Drpcp查询", serviceId = "99000002")
	public DemoQuery queryDrpcpService(DemoQuery req);
}

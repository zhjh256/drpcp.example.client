package com.medsoft.drpcp.api;

import java.util.List;

import com.medsoft.drpcp.meta.DrpcpHead;

public class DemoQuery extends DrpcpHead {
	private DemoReq param;
	private List<Demo> result;
	public DemoReq getParam() {
		return param;
	}
	public void setParam(DemoReq param) {
		this.param = param;
	}
	public List<Demo> getResult() {
		return result;
	}
	public void setResult(List<Demo> result) {
		this.result = result;
	}
}

package com.medsoft.drpcp.api;

import com.medsoft.drpcp.meta.DrpcpHead;

public class DemoResp extends DrpcpHead{
	private int errorNo;
	private String errorInfo;
	public int getErrorNo() {
		return errorNo;
	}
	public void setErrorNo(int errorNo) {
		this.errorNo = errorNo;
	}
	public String getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
}

package com.medsoft.drpcp.api;

import com.medsoft.drpcp.meta.DrpcpHead;

public class DemoReq extends DrpcpHead {
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
